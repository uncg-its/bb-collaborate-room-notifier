<?php
// *************************
// collaborate room notifier
// *************************
// notifies users via e-mail of pending room expiration, and provides instructions and links to renew.

require("config.php"); // config options in here.

// shortcut for dev mode
define("DEV_MODE", Config::getEnvironment() == "dev");

require("lib.php"); // helper functions and objects in here.

// query for who is not expired yet, upper limit = highest bound of $intervals

$upperLimit = clone end(array_values($checkDates)); // new DateTime object
$padding = "1 day";
$upperLimit->add(date_interval_create_from_date_string($padding)); // now the upper limit is 1 day past the highest threshold check

$upperLimitSql = $upperLimit->format(Config::TIMESTAMP_FORMAT);

$query = "SELECT * FROM rooms WHERE r_expire_date > NOW() AND r_expire_date <= '$upperLimitSql'";

// get the results
try {
	$host = Config::DB_HOST;
	$database = Config::DB_NAME;
	$dbh = new PDO("mysql:host=$host;dbname=$database", Config::DB_USER, Config::DB_PASSWORD);
	$stmt = $dbh->prepare($query);
	$stmt->execute();
	$results = $stmt->fetchAll();
	$dbh = null;
} catch (PDOException $e) {
	$msg = 'Database error while fetching: ' . $e->getMessage();
	error_log($msg);
	die($msg);
}

// start output if we're in "dev" environment
if (DEV_MODE) {
	echo "<h1>Dev Mode: data dump</h1>";
}

// iterate through rows to find expiring rooms.
foreach($results as $row) {
	// instantiate room object and populate
	$room = new Room($row);

	$exp = $room->expireDate;
	foreach($checkDates as $int => $dt) {
		if ($exp <= $dt) { // expiration date <= one of the test cases.
			// has the alert already been sent?
			$notificationsSent = $room->sentNotifications;
			if (!in_array($int, $notificationsSent)) {
				sendMail($room, $int);
				break; // skip the other checks, since they'll check out too.
			}
		}
	}
}

?>