<?php
function dump($var) {
	// returns preformatted data dump - for debugging
	echo "<pre>";
	die(print_r($var, true));
}

/**
 * Room class
 *
 * Contains properties of a room - can be passed back and forth in functions, etc.
 *
 */

class Room {
	// public vars OK here for now....not going to worry too much.
	public $dbId;
	public $name;
	public $contactName;
	public $primaryEmail;
	public $secondaryEmails;
	public $expireDate;

	public $sentNotifications = array();

	public function __construct($row) {
		extract($row);
		// takes in row from SQL and sets properties
		$this->dbId = $r_id;
		$this->name = $r_name;
		$this->contactName = "$r_firstname $r_lastname";
		$this->primaryEmail = $r_primary_email;
		$this->secondaryEmails = explode(",", str_replace(" ", "", $r_secondary_emails));
		$this->expireDate = new DateTime($r_expire_date, Config::getTimeZone());

		// fetch any sent notifications for this room.
		try {
			$query = "SELECT * FROM notifications WHERE r_id = {$this->dbId}";

			$host = Config::DB_HOST;
			$database = Config::DB_NAME;
			$dbh = new PDO("mysql:host=$host;dbname=$database", Config::DB_USER, Config::DB_PASSWORD);
			$stmt = $dbh->prepare($query);
			$stmt->execute();
			$results = $stmt->fetchAll();
			$dbh = null;
		} catch (PDOException $e) {
			$msg = 'Database error while fetching: ' . $e->getMessage();
			error_log($msg);
			die($msg);
		}

		// make nice and pretty in an array
		foreach($results as $index => $cols) {
			$this->sentNotifications[] = $cols['n_interval'];
		}

		// dump($this->sentNotifications);

	}
}

// -------------------------------------------------------------------------------------------------

// error reporting - dev
if (DEV_MODE) {
	error_reporting(E_ALL);
	ini_set('display_errors', 1);
}

// set up PHPMailer
require("PHPMailer/PHPMailerAutoload.php");

// determine comparison dates
$intervals = Config::getIntervals();
$checkDates = array();
foreach($intervals as $i) {
	$checkDates[$i] = new DateTime("now", Config::getTimeZone());
	$di = date_interval_create_from_date_string($i);
	$checkDates[$i]->add($di); // $checkDates now has another DateTime object to which we can compare later.
}
// sort soonest first (maintain key relationships)
asort($checkDates);

/**
 * sendMail()
 *
 * Sends an e-mail to the designated recipients alerting them of impending room expiration.
 * Logs each successful e-mail in database 'notifications' table
 * Failure to e-mail will write a log to the PHP error_log file.
 *
 * @param  Room			$room				Room object (containing all pertinent data)
 * @param  string		$expireInterval		Text version of which date threshold is being hit (e.g. "1 month")
 * @return null
 */

// function sendMail($emails, $roomName, $expireInterval, $expireDateTime) {
function sendMail($room, $expireInterval) {
	$ccText = "";

	$emails = array($room->primaryEmail);
	if (!empty($room->secondaryEmails[0])) {
		$secondaryEmails = $room->secondaryEmails;
		foreach($secondaryEmails as $address) {
			$emails[] = $address;
		}

		$ccText = "<br />(cc: " . implode(", ", $secondaryEmails) . ")";

	}

	// convert here so that original can be used in logs.
	$expFormat = $room->expireDate->format(Config::WRITTENDATE_FORMAT);

	$msg = "<p>Dear {$room->primaryEmail},$ccText</p>
	<p>You are receiving this message as a courtesy, because you are listed as a contact for the Blackboard Collaborate room titled <strong>{$room->name}</strong>. This room is set to expire in less than <strong>$expireInterval</strong>, on $expFormat.</p>
	<p>If you wish to extend this expiration date for another 1-year period, please contact 6-TECH (256-TECH or <a href=\"mailto:6-tech@uncg.edu\">6-tech@uncg.edu</a>). Please be sure to provide the name of the room. If you do not wish to extend the expiration date, you can simply do nothing and the room will expire as planned.</p>

	<p>As always, if you have questions about Blackboard Collaborate, please feel free to visit the ITS website (<a href=\"http://its.uncg.edu/Blackboard/Collaborate/\">http://its.uncg.edu/Blackboard/Collaborate/</a>) or contact 6-TECH.</p>

	<p>Best regards, <br />-- UNCG Blackboard Collaborate Admin</p>
	";

	$msgNoTags = strip_tags($msg);

	if (!is_array($emails)) {$emails = array($emails);}
	$mail = new PHPMailer;

	$mail->From = 'slms-admins-l@uncg.edu';
	$mail->FromName = "UNCG Blackboard Collaborate Administrator";

	foreach($emails as $email) {
		$mail->addAddress($email, "Collaborate User");
	}

	$mail->addBCC("slms-admins-l@uncg.edu", "SLMS Admins");
	$mail->addReplyTo("slms-admins-l@uncg.edu", "SLMS Admins");
	$mail->isHTML(true);

	$mail->Subject = "Blackboard Collaborate Room Expiration Notice";
	$mail->Body = $msg;
	$mail->AltBody = $msgNoTags;

	// prep log data
	$emailTimestamp = new DateTime("now", Config::getTimeZone());
	$emailTimestamp = $emailTimestamp->format(Config::TIMESTAMP_FORMAT);

	$emailRecipients = array();
	foreach($mail->getToAddresses() as $k => $v) {
		$emailRecipients[] = $v[0];
	}
	$emailRecipients = implode(";", $emailRecipients); // use ; to avoid possible confusion with the csv format.

	if (!DEV_MODE) {
		if (!$mail->send()) {
			error_log("Email error - message could not be sent: " . $mail->ErrorInfo);
		} else {
			// log used to be here. not anymore. just doing nothing.
		}
	} else {
		// DEV MODE; display on screen
		echo "
		<h3>MESSAGE:</h3>
		<p>To: $emailRecipients</p>
		{$mail->Body}
		<hr>";
	}

	// INSERT new row into notifications table.
	try {
		$host = Config::DB_HOST;
		$database = Config::DB_NAME;
		$dbh = new PDO("mysql:host=$host;dbname=$database", Config::DB_USER, Config::DB_PASSWORD);

		$sql = "INSERT INTO notifications(n_timestamp, n_interval, r_id) VALUES(NOW(), :interval, :r_id)";
		$stmt = $dbh->prepare($sql);
		// die("Interval: $expireInterval; RoomID: {$room->dbId}");
		$stmt->execute(array(":interval"=>$expireInterval, ":r_id"=>$room->dbId));
		$dbh = null;
	} catch (PDOException $e) {
		$msg = 'Database error while writing log: ' . $e->getMessage();
		error_log($msg);
		die($msg);
	}
}
?>