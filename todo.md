# Collaborate Room Notifier TODO

** TODO **
- Change logging location to a database table instead of a CSV file.


** FOR LATER **
- consider abstracting the e-mail function to use for logging, etc.
- Consider a regular "room status" email instead / in addition to the expiration notices?
- how to handle messages?  Right now primary & secondary are all To:.  Should we do primary as To: and secondaries as CC:?  Names are all listed as Collaborate User for now as well.  We do not store names of secondary contacts.

** DONE **
- initial query - select smaller date range (perhaps move $intervals into Config class and reference that there?)
- refactor to make a Room object, so that we can pass the object to the sendMail() function
- Fallbacks - what if the cron job fails to run that day?