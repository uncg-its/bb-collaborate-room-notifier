-- phpMyAdmin SQL Dump
-- version 4.4.9
-- http://www.phpmyadmin.net
--
-- Host: localhost:8889
-- Generation Time: Jul 24, 2015 at 10:33 PM
-- Server version: 5.5.42
-- PHP Version: 5.6.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `collaborate-rooms`
--
DROP DATABASE `collaborate-rooms`;
CREATE DATABASE IF NOT EXISTS `collaborate-rooms` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE `collaborate-rooms`;

-- --------------------------------------------------------

--
-- Table structure for table `rooms`
--

CREATE TABLE `rooms` (
  `r_id` int(11) NOT NULL,
  `r_name` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `r_lastname` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `r_firstname` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `r_primary_email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `r_secondary_emails` text COLLATE utf8_unicode_ci,
  `r_expire_date` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Truncate table before insert `rooms`
--

TRUNCATE TABLE `rooms`;
--
-- Dumping data for table `rooms`
--

INSERT INTO `rooms` (`r_id`, `r_name`, `r_lastname`, `r_firstname`, `r_primary_email`, `r_secondary_emails`, `r_expire_date`) VALUES
(1, 'BRYAN 114 TESTING', 'Cavenaugh', 'Sara', 'sncavena@uncg.edu', '', '2015-12-31 00:00:00'),
(2, 'CAREER CONVERSATIONS', 'Madsen', 'Patrick', 'pomadsen@uncg.edu', '', '2015-12-31 00:00:00'),
(3, 'CED 780 SUPERVISION', 'Wester', 'Kelly', 'klwester@uncg.edu', '', '2015-12-31 00:00:00'),
(4, 'CLINICAL TEACHER ACADEMY', 'O''Connor', 'Christina', 'ckoconno@uncg.edu', '', '2015-12-31 00:00:00'),
(5, 'CONFLICT TRANSFORMATION LEADERSHIP TRAINING', 'Schmitz', 'Cathryne', 'clschmit@uncg.edu', '', '2015-12-31 00:00:00'),
(6, 'DTS/DTC MEETING', 'Tyler', 'Maurice', 'matyler@uncg.edu', 'caroys@uncg.edu', '2015-12-31 00:00:00'),
(7, 'FOOD AND AGRICULTURAL NANOTECH', 'Tajkarimi', 'Mehrdad', 'm_tajkar@uncg.edu', '', '2015-12-31 00:00:00'),
(8, 'GROUP MEETING (BRYAN SCHOOL)', 'Black', 'Aprille', 'anblack2@uncg.edu', '', '2015-12-31 00:00:00'),
(9, 'HEALTH COACH TRAINING PROGRAMS', 'Pulliam', 'Regina', 'rlmccoy@uncg.edu', 'mslibera@uncg.edu', '2015-08-14 00:00:00'),
(10, 'HHS INSTRUCTIONAL TECHNOLOGY', 'Harris', 'Jane', 'jdharri5@uncg.edu', 'msl_adm@uncg.edu,mslibera@uncg.edu', '2015-08-01 00:00:00'),
(11, 'ITS HELPDESK: COLLABORATE PRACTICE', 'Harrison', 'Brittany', 'btharri2@uncg.edu', '', '2015-12-31 00:00:00'),
(12, 'LIBERA TEST ROOM', 'Libera', 'Matt', 'mslibera@uncg.edu', '', '2016-07-06 00:00:00'),
(13, 'LIS ROOM', 'Shiflett', 'Lee', 'olshifle@uncg.edu', 'njbird@uncg.edu,g_hoffma@uncg.edu', '2015-12-31 00:00:00'),
(14, 'LSTA MAKERSPACE', 'Filar Williams', 'Beth', 'efwilli3@uncg.edu', '', '2015-12-31 00:00:00'),
(15, 'MAKERSPACE', 'Vang', 'Touger', 't_vang@uncg.edu', '', '2015-12-31 00:00:00'),
(16, 'NCLA WEBINARS', 'Kellam', 'Lynda', 'lmkellam@uncg.edu', '', '2016-06-01 00:00:00'),
(17, 'ODM', 'Lanzer', 'Michelle', 'mslanzer@uncg.edu', 'mdsanfor@uncg.edu,clnorris@uncg.edu', '2016-01-31 00:00:00'),
(18, 'PCS', 'Chastain', 'Janeen', 'jkchast2@uncg.edu', '', '2015-07-31 00:00:00'),
(19, 'PCS ONLINE', 'Chastain', 'Janeen', 'jkchast2@uncg.edu', '', '2016-06-12 00:00:00'),
(20, 'PRECIOUS PREGNANCY STUDY', 'Cote-Arsenault', 'Denise', 'd_cotear@uncg.edu', '', '2015-12-31 00:00:00'),
(21, 'SCHOOL LIBRARY INFO SESSION', 'Akers', 'Anne', 'atakers@uncg.edu', '', '2015-12-31 00:00:00'),
(22, 'SOE ADVISING', 'Strickland', 'Brian', 'bjstrick@uncg.edu', '', '2015-12-31 00:00:00'),
(23, 'SOE ONLINE HELP DESK', 'Bates-Hart', 'Sanrda', 'smbatesh@uncg.edu', '', '2015-12-31 00:00:00'),
(24, 'SPEAKING CENTER', 'Ellis', 'Erin', 'edellis@uncg.edu', '', '2016-06-12 00:00:00'),
(25, 'TNT ROUND TABLE', 'Dale', 'Jenny', 'jedale2@uncg.edu', '', '2015-12-31 00:00:00'),
(26, 'UNCG - BEYOND ACADEMICS', 'Marshburn', 'Eric', 'e_marshb@uncg.edu', '', '2015-12-31 00:00:00'),
(27, 'UNCG LIBRARY', 'Lawrimore', 'Erin', 'erlawrim@uncg.edu', '', '2015-12-31 00:00:00'),
(28, 'UNCG SUMMER INTERNATIONAL UNDERGRADUATE RESEARCH PROGRAM', 'Nile', 'Terry', 'tanile@uncg.edu', '', '2016-05-09 00:00:00'),
(29, 'UNCG TELECLASSROOM', 'Ridenhour', 'Lane', 'l_ridenh@uncg.edu', '', '2016-05-31 00:00:00'),
(30, 'UNCG USABILITY TESTING', 'Brown', 'Laura', 'ljbrown3@uncg.edu', '', '2015-12-31 00:00:00'),
(31, 'UNIVERSITY LIBRARIES', 'Leininger', 'Lea', 'laleinin@uncg.edu', '', '2015-12-31 00:00:00'),
(32, 'X-CULTURE WEBINAR', 'Taras', 'Vasyl', 'v_taras@uncg.edu', '', '2015-12-31 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `rooms`
--
ALTER TABLE `rooms`
  ADD PRIMARY KEY (`r_id`),
  ADD UNIQUE KEY `r_id` (`r_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `rooms`
--
ALTER TABLE `rooms`
  MODIFY `r_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=33;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
