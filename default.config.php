<?php
/**
 * Config class
 *
 * Contains constants / variables that need to be accessible across the app.
 *
 */
class Config {

	// ********** user-defined values **********

	// database
	const DB_HOST = "";
	const DB_NAME = "";
	const DB_USER = "";
	const DB_PASSWORD = "";

	// timezone
	protected static $timezone = "America/New_York";

	// intervals to check
	protected static $intervals = array("1 month", "1 week", "1 day");

	// ********** app values - do not edit **********

	// date formats
	const WRITTENDATE_FORMAT = "l, F jS, Y \a\\t g:i a";
	const TIMESTAMP_FORMAT = "Y-m-d H:i:s";

	// ********** methods - do not edit **********

	// returns environment ("dev" or "prod")
	public static function getEnvironment() {
		$env = getenv("APPLICATION_ENV") == "" ? getopt("e:")["e"] : getenv("APPLICATION_ENV"); // need to do this because we want to support .htaccess for browser OR command line for cron
		$environment = ($env == "production") ? "prod" : "dev"; // assume dev unless specifically set to prod
		//die($environment . " " . getopt("e:")["e"] . "\n");
		return $environment;
	}

	// returns intervals array
	public static function getIntervals() {
		return Config::$intervals;
	}

	// returns DateTimeZone object
	public static function getTimeZone() {
		return new DateTimeZone(Config::$timezone);
	}
}