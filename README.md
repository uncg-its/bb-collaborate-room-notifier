## BB COLLABORATE ROOM NOTIFIER

### WHAT IS THIS?

    * Notification script used by UNCG to alert Collaborate room holders of pending room expiration
    * At UNCG, Collaborate v12.6 rooms are created all within one user account (UNCG-ADMIN) - this script uses data stored externally about the users who actually "own" the room and notifies them automatically via e-mail when a room will expire.
    * Version history is at the base of this README file

### WHO MAINTAINS THIS?

    * UNCG LMS Administrators, who are part of the Application Administration group within Information Technology Services

### QUESTIONS? COMMENTS? CONCERNS?

    * 6-TECH@uncg.edu or lms-admins-l@uncg.edu

### SETUP AND USE

    * Save `default.config.php` as `config.php` and update the first section with your own database and timezone details. Update intervals as you see fit.
    * In the `sql` folder you should find SQL code to create the necessary tables in your database. `notifications` table should be empty, but you will need to populate the `rooms` table with your own information. (GUI may be coming for this at some point.)
        * _ the `r_secondary_emails` field is comma-separated _
    * On the server, make sure that .htaccess includes `SetEnv APPLICATION_ENV "production"` on your production server, or else e-mail will not be sent. If `APPLICATION_ENV` is set to anything else, DEV_MODE will be on, which will output results to the screen instead.
        * sample `.htaccess-prod` file is included in repo
    * `cron.php` contains the actual script. That file should be run on a daily cron job.
        * with cPanel, we've had better luck using the cURL command instead of the php command...


### VERSION HISTORY (most recent first)

    v1.0
    -- Initial release. No GUI.

    pre-release v0.2.1 (rc2):
    -- separates out config information - now config.php contains only the Config class, wherein the user will change variables (database, timezone, etc.) to suit their own environment. other helpers and library functions/classes are now in lib.php.
    -- adds default.config.php for distribution (will not commit config.php to repo)
    -- adds how-to documentation (Setup and Use, above)
    -- moves active code to `cron.php` (still have a dummy `index.php`)

	pre-release v0.2 (rc1):
	-- notifications are now stored in a separate SQL table by room ID (r_id)
	-- additional tweaks for more flexibility in logging and

    pre-release v0.1:
    -- Rooms are stored in a SQL database
    -- Notifications are stored in a CSV file

### POSSIBLE FUTURE ENHANCEMENTS

	* Possible GUI for editing / adding records to database?
	* condense DB calls into another object
	* structure code base better - separate models from rest of code?